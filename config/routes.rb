Rails.application.routes.draw do
  resources :plans

  namespace :auth do
    resources :sessions
  end

  resources :membership_types
  resources :memberships
  resources :work_sessions
  resources :companies
  resources :purchases
  resources :products
  resources :users do
    member do
      get  :actions
      get :confirmation
      patch :update_confirmation
      get :manage_memberships
      get :manage
      get :set_pin
      patch :update_pin
      get :thank_you

    end
    resources :purchases
    resources :work_sessions do
      member do
        post :checkout
      end
      collection do
        post :checkin
      end
    end
  end
  resources :spaces do
    resources :space_products
  end

  get "/auth/auth0/callback" => "auth0#callback"
  get "/auth/failure" => "auth0#failure"


  post '/products/products', to: 'products#tablet'
  post '/tablet/products', to: 'products#tablet'
  post 'tablet/chargebee_products', to: 'products#tablet_chargebee'
  post '/push_cobot_purchase/purchases', to: 'purchases#push_cobot_purchase'
  post '/push_chargebee_purchases', to: 'purchases#push_chargebee_purchase'
  get '/tablet', to: 'users#tablet'
  get '/sign_up', to: 'users#sign_up'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'users#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end 
