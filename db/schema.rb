# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160731174012) do

  create_table "companies", force: :cascade do |t|
    t.string   "company_name"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "membership_types", force: :cascade do |t|
    t.string   "name"
    t.string   "plan_id"
    t.string   "product_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "memberships", force: :cascade do |t|
    t.string   "space_id"
    t.string   "user_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.integer  "membership_type_id"
  end

  create_table "plans", force: :cascade do |t|
    t.string   "name"
    t.string   "invoice_name",            limit: 100
    t.string   "description",             limit: 500
    t.integer  "price_cents"
    t.string   "currency_code",           limit: 3
    t.integer  "period"
    t.integer  "period_unit"
    t.integer  "trial_period"
    t.integer  "trial_period_unit"
    t.integer  "charge_model"
    t.integer  "free_quantity"
    t.decimal  "setup_cost"
    t.integer  "status"
    t.datetime "archived_at"
    t.integer  "billing_cycles"
    t.string   "redirect_url",            limit: 500
    t.boolean  "enabled_in_hosted_pages"
    t.boolean  "enabled_in_portal"
    t.string   "tax_code",                limit: 50
    t.string   "invoice_notes",           limit: 1000
    t.boolean  "taxable"
  end

  create_table "products", force: :cascade do |t|
    t.string   "invoice_name",      limit: 50
    t.string   "description",       limit: 500
    t.integer  "type"
    t.integer  "addon_type"
    t.integer  "price_cents"
    t.string   "currency_code",     limit: 3
    t.integer  "period"
    t.integer  "period_unit"
    t.string   "unit",              limit: 30
    t.integer  "status"
    t.datetime "archived_at"
    t.boolean  "enabled_in_portal"
    t.string   "tax_code",          limit: 50
    t.string   "invoice_notes",     limit: 1000
    t.boolean  "taxable"
    t.boolean  "meta_data"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.string   "name"
    t.integer  "product_type"
  end

  create_table "purchases", force: :cascade do |t|
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.integer  "user_id"
    t.integer  "space_product_id"
  end

  create_table "space_products", force: :cascade do |t|
    t.integer  "quantity"
    t.integer  "space_id"
    t.integer  "product_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "spaces", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "name"
  end

  create_table "users", force: :cascade do |t|
    t.string   "first_name",          limit: 150
    t.string   "last_name",           limit: 150
    t.string   "email",               limit: 70
    t.string   "phone",               limit: 50
    t.datetime "DOB"
    t.string   "company",             limit: 250
    t.string   "vat_number",          limit: 20
    t.integer  "auto_collection"
    t.boolean  "allow_direct_debit"
    t.datetime "created_at"
    t.string   "created_from_ip",     limit: 50
    t.integer  "taxability"
    t.integer  "entity_code"
    t.string   "exempt_number",       limit: 100
    t.string   "invoice_notes",       limit: 1000
    t.decimal  "promotional_credits"
    t.decimal  "refundable_credits"
    t.decimal  "excess_payments"
    t.datetime "start_date"
    t.integer  "tablet_pin",          limit: 4
    t.boolean  "agree_to_terms"
    t.string   "name"
  end

  create_table "work_sessions", force: :cascade do |t|
    t.datetime "start_time"
    t.datetime "end_time"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "space_id"
  end

end
