# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)



50.times do |u|
 User.create(name: "Malcolm Durling")
end

20.times do |u|
 Product.create(name: "Product #{u}")
end


Space.create(name: "Enklave Neukolln")

