class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :product_id
      t.string :invoice_name, limit: 50
      t.string :description, limit: 500
      t.integer :type
      t.integer :addon_type
      t.decimal :price, :precision => 8, :scale => 2
      t.string  :currency_code, limit: 3
      t.integer :period
      t.integer :period_unit
      t.string :unit, limit: 30
      t.integer :status
      t.datetime :archived_at
      t.boolean :enabled_in_portal
      t.string  :tax_code, limit: 50
      t.string :invoice_notes, limit: 1000
      t.boolean :taxable
      t.boolean :meta_data
      t.timestamps null: false
    end
  end
end
