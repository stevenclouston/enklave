class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :user_id
      t.string :first_name, limit: 150
      t.string :last_name, limit: 150
      t.string :email, limit:70
      t.string :phone, limit:50
      t.datetime :DOB
      t.string :company, limit:250
      t.string :vat_number, limit:20
      t.integer :auto_collection
      t.boolean :allow_direct_debit
      t.timestamp :created_at
      t.string :created_from_ip, limit: 50
      t.integer :taxability
      t.integer :entity_code
      t.string :exempt_number, limit: 100
      t.string :invoice_notes, limit: 1000
      t.decimal :promotional_credits
      t.decimal :refundable_credits
      t.decimal :excess_payments
      t.datetime :start_date
      t.integer :tablet_pin, :limit => 4
      t.boolean :agree_to_terms
    end
  end
end
