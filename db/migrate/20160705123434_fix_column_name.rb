class FixColumnName < ActiveRecord::Migration
  def change
    def self.up
      rename_column :users, :user_id, :name
    end

    def self.down
      # rename back if you need or do something else or do nothing
    end
  end
end