class ChangePlanPriceCol < ActiveRecord::Migration
  def change
    rename_column :plans, :price, :price_cents
    change_column :plans, :price_cents, :integer
  end
end
