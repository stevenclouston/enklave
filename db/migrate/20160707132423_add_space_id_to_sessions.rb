class AddSpaceIdToSessions < ActiveRecord::Migration
  def change
    add_column :work_sessions, :space_id, :integer
  end
end
