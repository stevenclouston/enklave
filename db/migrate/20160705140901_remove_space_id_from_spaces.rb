class RemoveSpaceIdFromSpaces < ActiveRecord::Migration
  def change
    remove_column :spaces, :space_id, :string
  end
end
