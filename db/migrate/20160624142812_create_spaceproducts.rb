class CreateSpaceproducts < ActiveRecord::Migration
  def change
    create_table :space_products do |t|
      t.integer :quantity
      t.integer :space_id
      t.integer :product_id
      t.timestamps null: false
    end
  end
end
