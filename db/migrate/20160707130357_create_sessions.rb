class CreateSessions < ActiveRecord::Migration
  def change
    create_table :work_sessions do |t|
      t.datetime :start_time
      t.datetime :end_time
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
