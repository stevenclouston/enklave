class ChangeProductPriceColumn < ActiveRecord::Migration
  def change
    rename_column :products, :price, :price_cents
    change_column :products, :price_cents, :integer
  end
end
