class CreatePlans < ActiveRecord::Migration
  def change
    create_table :plans do |t|
      t.string :name
      t.string :invoice_name, limit: 100
      t.string :description, limit: 500
      t.decimal :price
      t.string :currency_code, limit: 3
      t.integer :period
      t.integer :period_unit
      t.integer :trial_period
      t.integer :trial_period_unit
      t.integer :charge_model
      t.integer :free_quantity
      t.decimal :setup_cost
      t.integer :status
      t.datetime :archived_at
      t.integer :billing_cycles
      t.string :redirect_url, limit: 500
      t.boolean :enabled_in_hosted_pages
      t.boolean :enabled_in_portal
      t.string :tax_code, limit: 50
      t.string :invoice_notes, limit: 1000
      t.boolean :taxable
    end
  end
end
