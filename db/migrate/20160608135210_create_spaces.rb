class CreateSpaces < ActiveRecord::Migration
  def change
    create_table :spaces do |t|
      t.string :space_id

      t.timestamps null: false
    end
  end
end
