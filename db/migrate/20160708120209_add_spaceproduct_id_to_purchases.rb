class AddSpaceproductIdToPurchases < ActiveRecord::Migration
  def change
    add_column :purchases, :spaceproduct_id, :integer
  end
end
