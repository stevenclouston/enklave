class PlansApiService
  def initialize(plan)
    @plan = plan;
  end

  def create

    result = ChargeBee::Plan.create(params);
    Rails.logger.info result
  end

  def update
    result = ChargeBee::Plan.update(@product);
    Rails.logger.info result
  end

  def delete
    result = ChargeBee::Addon.delete(@product[:id]);
    Rails.logger.info result
  end

  private
  def params
    {
        id: @plan["id"].to_s,
        name: @plan["name"],
        invoice_name: @plan["invoice_name"],
        description: @plan["description"],
        price: @plan["price_cents"],
    }
  end
end
