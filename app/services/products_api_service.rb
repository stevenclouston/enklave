class ProductsApiService
  def initialize(product)
    @product = product;
  end

  def create
    result = ChargeBee::Addon.create(params);
    Rails.logger.info result
  end

  def update
    result = ChargeBee::Addon.update(@product);
    Rails.logger.info result
  end

  def delete
    result = ChargeBee::Addon.delete(@product[:id]);
    Rails.logger.info result
  end

  private
  def params
    {
      id: @product["id"].to_s,
      name: @product["name"],
      invoice_name: @product["name"],
      charge_type: "recurring",
      price: 200,
      period: 1,
      period_unit: "month",
      type: "on_off",
    }
  end

end
