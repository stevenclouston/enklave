
class SubscriptionApiService
  def initialize(product)
    @product = product;
  end

  def create
    result = ChargeBee::Addon.create(params);
    Rails.logger.info result
  end

  def update
    result = ChargeBee::Addon.update(@product);
    Rails.logger.info result
  end

  def delete
    result = ChargeBee::Addon.delete(@product[:id]);
    Rails.logger.info result
  end

  private
  def params
    {
        "id": "8avVGOkx8U1MX",
        "customer_id": "8avVGOkx8U1MX",
        "plan_id": "basic",
        "plan_quantity": 1,
        "status": "non_renewing",
        "trial_start": 1412101835,
        "trial_end": 1414780235,
        "current_term_start": 1467311435,
        "current_term_end": 1469989835,
        "remaining_billing_cycles": 0,
        "created_at": 1412101835,
        "started_at": 1412101835,
        "activated_at": 1414780235,
        "cancelled_at": 1469989835,
        "has_scheduled_changes": false,
    "object": "subscription",
        "currency_code": "USD",
        "due_invoices_count": 2,
        "due_since": 1438367435,
        "total_dues": 1800,
        "shipping_address": {
        "first_name": "Benjamin",
        "last_name": "Ross",
        "company": "Acme Inc",
        "phone": "+1 (614) 226-4809",
        "line1": "345, Redington Av",
        "line2": "Suite 1200",
        "city": "Los Angeles",
        "state_code": "CA",
        "state": "California",
        "country": "US",
        "object": "shipping_address"
    }
    }
  end

end
