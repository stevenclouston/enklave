require 'auth0'

class Auth0Service

  def initialize(user)
    @user = user
    @auth0 = Auth0Client.new(
      client_id: ENV["AUTH0_CLIENT_ID"],
      token: ENV["AUTH0_API_TOKEN"],
      domain: ENV["AUTH0_DOMAIN"],
    )
  end

  def create_user
    @auth0.create_user(name, params)
  end

  private

  def name
    "#{@user["first_name"]} #{@user["last_name"]}"
  end

  def params
    {
      connection: "Username-Password-Authentication",
      email: @user["email"],
      password: @user["email"],
      user_metadata: { id: @user["id"] }
    }
  end

end
