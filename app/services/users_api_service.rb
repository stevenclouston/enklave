class UsersApiService
  def initialize(user)
    @user = user;
  end

  def create
    my_test_params = params
    result = ChargeBee::Customer.create(params);
    Rails.logger.info result
  end

  def update
    result = ChargeBee::Customer.update(params);
    Rails.logger.info result
  end

  def delete
    result = ChargeBee::Customer.delete(@user[:id]);
    Rails.logger.info result
  end


  private

  def params
    {
        id: @user["id"],
        first_name: @user["first_name"],
        last_name: @user["last_name"],
        email: @user["email"],
    }
  end
end
