$( document ).ready(function() {
  
  $( ".PINPad" ).click(function(){
   var button = $(this);
   addNumber(button.val());
  })

  $( "#PINClear" ).click(function(){
   clearForm();
  })

  $( "#PINSubmit" ).click(function(){
   submitForm();
  })

  function addNumber(value){
    var v = $( "#PINbox" ).val();
    $( "#PINbox" ).val( v + value );
  }

  function clearForm(){
    $( "#PINbox" ).val( "" );
  }

});
