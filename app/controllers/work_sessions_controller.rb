class WorkSessionsController < ApplicationController
  skip_before_filter :logged_in_using_omniauth?
  before_action :set_work_session, only: [:show, :edit, :update, :destroy, :checkout]

  def index
    @work_sessions = WorkSession.all
  end

  def show
  end

  def new
    @user = User.find(params[:user_id])
  end

  def edit
  end

  def create
    @work_session = WorkSession.new(work_session_params)

    respond_to do |format|
      if @work_session.save
        format.html { redirect_to edit_user_path(@work_session.user), notice: 'Session was successfully created.' }
        format.json { render :show, status: :created, location: @work_session }
      else
        format.html { render :new }
        format.json { render json: @work_session.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @work_session.update(work_session_params)
        format.html { redirect_to @work_session, notice: 'Work Session was successfully updated.' }
        format.json { render :show, status: :ok, location: @work_session }
      else
        format.html { render :edit }
        format.json { render json: @work_session.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @work_session.destroy
    respond_to do |format|
      format.html { redirect_to work_sessions_url, notice: 'Work Session was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def checkout
    @work_session.set_end_time
    respond_to do |format|
      flash[:success] = "You have been checked out successfully."
      format.html { redirect_to '/tablet' }
      format.json { head :no_content }
    end
  end

  def checkin
    @work_session = WorkSession.create(user_id: params[:user_id], space_id: Space.first.id )
    @work_session.set_start_time
    respond_to do |format|
      flash[:success] = "You have been checked in successfully."
      format.html { redirect_to '/tablet' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_work_session
      @user = User.find(params[:user_id])
      @work_session = WorkSession.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def work_session_params
      params.require(:work_session).permit(:start_time, :end_time, :user_id, :space_id)
    end
end
