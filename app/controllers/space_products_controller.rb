class SpaceProductsController < ApplicationController
  before_action :set_products, only: [:index]

  def create
    space = Space.find(params[:space_id])
    product = Product.find(params[:product_id])
    space.products << product
    redirect_to space, notice: 'Product was successfully associated.'
  end

  def index

  end

  def set_products
      @space = Space.find(params[:space_id])
      @associated_products = @space.products
      @all_products = Product.all
      @non_associated_products = @all_products - @associated_products
  end

end