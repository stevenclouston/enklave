class UsersController < ApplicationController
  skip_before_filter :logged_in_using_omniauth?
  before_action :set_user, only: [:show, :edit, :update, :destroy, :actions, :confirmation, :update_confirmation]


  # GET /users
  # GET /users.json
  def index
    @users = User.all # User.all
  end

  def tablet
    @users = User.all
  end

  # GET /users/1
  # GET /users/1.json
  def show
  end

  # GET /users/new
  def new

    @user = User.new
  end

  def set_pin

    @user= User.find(params[:id])
  end
  # GET /users/1/edit
  def edit
    @work_session = @user.active_session || WorkSession.new
  end

  def manage_memberships
    @membership_types = MembershipType.all
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)
    respond_to do |format|
      if @user.save
        #UsersApiService.new(@user.attributes).create
        #Auth0Service.new(@user.attributes).create_user
        format.html { redirect_to new_membership_path(:user_id => @user.id) }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def sign_up
    @user = User.new
  end
  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update_pin
    @user = User.find(params[:id])
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to thank_you_user_path, notice: 'User pin was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end
  def update
    respond_to do |format|
      if @user.update(user_params)
        UsersApiService.new(@user.attributes).update
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    #UsersApiService.new(@user.attributes).delete
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def actions
    # this should be made specific to the current space
    @products = Space.first.space_products
  end


  def confirmation

    if params[:step].present?
      @step = params[:step].to_i
    else
      @step = 1
    end
  end

  def update_confirmation
      @step = params[:step]
      if @user.update(user_params)
        #UsersApiService.new(@user.attributes).update
        @step = @step.to_i + 1
        #UsersApiService.new(@user.attributes).update
        if @step <=8
          redirect_to confirmation_user_path(@user,  step: @step)
        else
          redirect_to users_path()
        end
      else
        render :confirmation
      end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:first_name, :last_name, :billing_first_name, :billing_last_name, :billing_address_line_1,:billing_address_city, :billing_address_state, :billing_address_zip, :billing_address_country, :current_address_line_1, :current_address_city, :current_address_state, :current_address_zip, :current_address_country, :email, :cell_phone, :auto_collection, :allow_direct_debit, :taxability, :object, :card_status, :promotional_credits, :refundable_credits, :excess_payments, :tablet_pin )
    end
end


def cobot_memberships
  cobot_client.get('etest', '/memberships', attributes: 'name,id')
end
def cobot_client
  CobotClient::ApiClient.new 'd1afe640346642dcb95b40dad20a0d0e118a3a390e523074b1d65df3f17f1b4d'
end

def chargebee_client

end
def chargebee_members
  require 'chargebee'
  ChargeBee.configure(:site => "steventest-test",
                      :api_key => "test_apcdcdFLAJuN2OrjRYvcdQFpbakBg6dszQk")
  list1 = ChargeBee::Customer.list()
end
