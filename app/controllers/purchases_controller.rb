class PurchasesController < ApplicationController
  before_action :set_purchase, only: [:show, :edit, :update, :destroy, :products]
  before_action :set_products, only: [:index]

  def new
    @user = User.find(params[:user_id])
    @space_product = SpaceProduct. find(params[:space_product_id])
  end

  def create
    user = User.find(params[:user_id])
    space_product = SpaceProduct.find(params[:space_product_id])
    user.space_products << space_product
    #PurchasesApiService.new().create
    flash[:success] = "Your purchase of #{space_product.product.name} was successful."
    redirect_to '/tablet'
  end

  def index

  end

  def set_products
    @user = User.find(params[:user_id])
    @space =  Space.find(@user.active_session.space_id)
    @purchases = @user.purchases
    @all_space_products = @space.space_products
  end

  # GET /purchases
  # GET /purchases.json

  # GET /purchases/1
  # GET /purchases/1.json
  def show
    @purchase = Purchase.find(params[:id])
    @product = @purchase.product
    @user = @purchase.user
  end

  # GET /purchases/new


  # GET /purchases/1/edit
  def edit
  end

  # POST /purchases
  # POST /purchases.json

  # PATCH/PUT /purchases/1
  # PATCH/PUT /purchases/1.json
  def update
    respond_to do |format|
      if @purchase.update(purchase_params)
        #PurchasesApiService.new(@purchase.attributes).update
        format.html { redirect_to @purchase, notice: 'Purchase was successfully updated.' }
        format.json { render :show, status: :ok, location: @purchase }
      else
        format.html { render :edit }
        format.json { render json: @purchase.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /purchases/1
  # DELETE /purchases/1.json
  def destroy
    @user = @purchase.user
    @purchase.destroy
    #PurchasesApiService.new(@purchase.attributes).delete
    respond_to do |format|
      format.html { redirect_to user_purchases_path(@user), notice: 'Purchase was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_purchase
      @purchase = Purchase.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def purchase_params
      params.fetch(:purchase, {})
    end
end
