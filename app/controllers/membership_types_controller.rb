class MembershipTypesController < ApplicationController
  skip_before_filter :logged_in_using_omniauth?
  before_action :set_membership_type, only: [:show, :edit, :update, :destroy]

  # GET /membership_types
  # GET /membership_types.json
  def index
    @membership_types = MembershipType.all

  end

  # GET /membership_types/1
  # GET /membership_types/1.json
  def show
  end

  # GET /membership_types/new
  def new
    @membership_type = MembershipType.new
    @membership_products = Product.membership
    @plan = Plan.all
  end



  # GET /membership_types/1/edit
  def edit
    @products = Product.all
    @membership_products = Product.membership_products
    @plan = Plan.all
  end

  # POST /membership_types
  # POST /membership_types.json
  def create
    @membership_type = MembershipType.new(membership_type_params)

    respond_to do |format|
      if @membership_type.save
        format.html { redirect_to @membership_type, notice: 'Membership type was successfully created.' }
        format.json { render :show, status: :created, location: @membership_type }
      else
        format.html { render :new }
        format.json { render json: @membership_type.errors, status: :unprocessable_entity }
      end
    end
  end



  # PATCH/PUT /membership_types/1
  # PATCH/PUT /membership_types/1.json
  def update
    respond_to do |format|
      if @membership_type.update(membership_type_params)
        format.html { redirect_to @membership_type, notice: 'Membership type was successfully updated.' }
        format.json { render :show, status: :ok, location: @membership_type }
      else
        format.html { render :edit }
        format.json { render json: @membership_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /membership_types/1
  # DELETE /membership_types/1.json
  def destroy
    @membership_type.destroy
    respond_to do |format|
      format.html { redirect_to membership_types_url, notice: 'Membership type was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_membership_type
      @membership_type = MembershipType.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def membership_type_params
      params.fetch(:membership_type, {}).permit(:plan_id, :product_id, :name)
    end
end
