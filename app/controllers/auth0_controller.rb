class Auth0Controller < ApplicationController
  def callback
    # This stores all the user information that came from Auth0
    # and the IdP
    session[:userinfo] = User.find(user_id)

    # Redirect to the URL you want after successfull auth
    redirect_to '/'
  end

  def failure
    # show a failure page or redirect to an error page
    @error_msg = request.params['message']
  end

  private

  def user_id
    request.env['omniauth.auth'].extra.raw_info.user_metadata.id
  end
end
