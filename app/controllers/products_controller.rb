class ProductsController < ApplicationController
  skip_before_filter :logged_in_using_omniauth?
  before_action :set_product, only: [:show, :edit, :update, :destroy]
  before_action :set_products, only: [:index]

  def tablet
    @tablet_products = cobot_products
    @member = params

  end

  def tablet_chargebee
    @chargebee_tablet_products = chargebee_products
    @member = params
  end

  # GET /products
  # GET /products.json
  def index
  end

  # GET /products/1
  # GET /products/1.json
  def show
  end

  # GET /products/new
  def new
    @product = Product.new

  end

  # GET /products/1/edit
  def edit
    @product_type = Product.find(params[:id]).product_type
    #if @product_type.nil? then @product_type = "not set" end
  end

  # POST /products
  # POST /products.json
  def create
    @product = Product.new(product_params)

    respond_to do |format|
      if @product.save
        ProductsApiService.new(@product.attributes).create
        #ProductsApiService.new(@product.attributes).create
        format.html { redirect_to @product, notice: 'Product was successfully created.' }
        format.json { render :show, status: :created, location: @product }
      else
        format.html { render :new }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /products/1
  # PATCH/PUT /products/1.json
  def update
    respond_to do |format|
      if @product.update(product_params)
        #ProductsApiService.new(@product.attributes).update
        format.html { redirect_to @product, notice: 'Product was successfully updated.' }
        format.json { render :show, status: :ok, location: @product }
      else
        format.html { render :edit }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /products/1
  # DELETE /products/1.json
  def destroy
    @product.destroy
    #ProductsApiService.new(@product.attributes).delete
    respond_to do |format|
      format.html { redirect_to products_url, notice: 'Product was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product
      @product = Product.find(params[:id])
    end

    def set_products
      if params[:space_id].present?
        @space = Space.find(params[:space_id])
        @products = @space.products
      else
        @products = Product.all
      end
    end


    # Never trust parameters from the scary internet, only allow the white list through.
    def product_params
      params.require(:product).permit(:name, :product_type, :price)
    end
end

def cobot_products
  cobot_client.get('etest', '/resources', attributes: 'name,id')
end

def cobot_client
  CobotClient::ApiClient.new '92dc92196ce9fedbf18f5d759c98abee3ea01e0fa320d90f5146578a593014a9'
end

def chargebee_products
  require 'chargebee'
  ChargeBee.configure(:site => "steventest-test",
                      :api_key => "test_apcdcdFLAJuN2OrjRYvcdQFpbakBg6dszQk")
  list = ChargeBee::Addon.list
end
