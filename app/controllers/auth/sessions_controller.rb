module Auth
  class SessionsController < ApplicationController
    skip_before_filter :logged_in_using_omniauth?

    def new
    end

    def create
    end

    def destroy
      session[:userinfo] = nil
      redirect_to new_auth_session_path
    end
  end
end
