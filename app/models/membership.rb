class Membership < ActiveRecord::Base
  belongs_to :space
  belongs_to :user
  has_one :membership_type
end
