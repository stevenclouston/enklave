class WorkSession < ActiveRecord::Base
  after_create :set_start_time

  belongs_to :user
  belongs_to :space

  def set_start_time
    update_attribute(:start_time, Time.now)
  end

  def set_end_time
    update_attribute(:end_time, Time.now)
  end
end
