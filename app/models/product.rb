class Product < ActiveRecord::Base
  has_many :space_products
  has_many :spaces, through: :space_products
  has_many :membership_types, dependent: :nullify

  monetize :price_cents, as: :price, :disable_validation => true

  enum product_type: [:consumable, :membership]
  enum type: [:on_off, :quantity] #refer to chargebee documentation
  enum charge_type: [:recurring, :non_recurring]
  enum period_unit: [:week, :month, :year, :not_applicable]
  enum status: [:active, :archived, :deleted]
  has_one :company

end

