class Space < ActiveRecord::Base
  has_many :space_products
  has_many :products, through: :space_products
end
