class Plan < ActiveRecord::Base
  has_many :membership_types, dependent: :nullify

  monetize :price_cents, as: :price, :disable_validation => true
  enum period_unit: [:week, :month, :year]
  enum charge_model: [:flat_fee, :per_unit]
  enum status: [:active, :archived, :deleted]
end
