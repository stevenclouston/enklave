class MembershipType < ActiveRecord::Base
  belongs_to :membership
  belongs_to :product
  belongs_to :plan
end
