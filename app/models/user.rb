class User < ActiveRecord::Base
  has_many :purchases
  has_many :space_products, through: :purchases
  has_many :work_sessions

  enum auto_collection: [:on, :off]
  enum taxability: [:taxable, :exempt]
  enum entity_code: [:a, :b, :c, :d, :e, :f, :g, :h, :i, :j, :k, :l, :m, :n, :o, :p, :q, :r, :med1, :med2]

  def active_session
    work_sessions.find_by(end_time: nil)
  end
end
