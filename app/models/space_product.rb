class SpaceProduct < ActiveRecord::Base
  belongs_to :space
  belongs_to :product
  has_many :purchases
  has_many :users, through: :purchases
end
