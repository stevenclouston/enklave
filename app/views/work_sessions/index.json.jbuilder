json.array!(@work_sessions) do |work_sessions|
  json.extract! work_sessions, :id, :start_time, :end_time, :user_id
  json.url work_session_url(work_session, format: :json)
end
