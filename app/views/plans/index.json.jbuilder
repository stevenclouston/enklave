json.array!(@plans) do |plan|
  json.extract! plan, :id, :name, :invoice_name, :description, :price, :period_unit, :trial_period, :trial_period_unit, :charge_model, :redirect_url, :tax_code
  json.url plan_url(plan, format: :json)
end
