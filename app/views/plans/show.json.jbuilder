json.extract! @plan, :id, :name, :invoice_name, :description, :price, :period_unit, :trial_period, :trial_period_unit, :charge_model, :redirect_url, :tax_code, :created_at, :updated_at
