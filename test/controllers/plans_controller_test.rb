require 'test_helper'

class PlansControllerTest < ActionController::TestCase
  setup do
    @plan = plans(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:plans)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create plan" do
    assert_difference('Plan.count') do
      post :create, plan: { charge_model: @plan.charge_model, description: @plan.description, invoice_name: @plan.invoice_name, name: @plan.name, period_unit: @plan.period_unit, price: @plan.price, redirect_url: @plan.redirect_url, tax_code: @plan.tax_code, trial_period: @plan.trial_period, trial_period_unit: @plan.trial_period_unit }
    end

    assert_redirected_to plan_path(assigns(:plan))
  end

  test "should show plan" do
    get :show, id: @plan
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @plan
    assert_response :success
  end

  test "should update plan" do
    patch :update, id: @plan, plan: { charge_model: @plan.charge_model, description: @plan.description, invoice_name: @plan.invoice_name, name: @plan.name, period_unit: @plan.period_unit, price: @plan.price, redirect_url: @plan.redirect_url, tax_code: @plan.tax_code, trial_period: @plan.trial_period, trial_period_unit: @plan.trial_period_unit }
    assert_redirected_to plan_path(assigns(:plan))
  end

  test "should destroy plan" do
    assert_difference('Plan.count', -1) do
      delete :destroy, id: @plan
    end

    assert_redirected_to plans_path
  end
end
